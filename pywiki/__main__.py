from time import sleep

from . import cli
from .core.builder import WikiBuilder


if __name__ == "__main__":
    args = cli.get_args()
    if args.auto:
        WikiBuilder(args).start_autobuild()
    else:
        WikiBuilder(args).build_wiki()
