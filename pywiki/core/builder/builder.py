import os
from pathlib import Path

from pywiki.config import Config

from pywiki.core.utils.filesys import get_folders_files, get_folders_subdirs
from pywiki.core.observer import WikiTreeFilesWatcher

from .md_html import is_md_file, add_page_file, md2html_extension, add_index_page


class WikiBuilder:
    def __init__(self, args) -> None:
        self.args = args

    def build_wiki(self) -> None:
        print(":: build updated")
        self._update_config()
        self._drop_output_folders()
        self._create_output_folders()
        self._copy_styles_by_theme()
        self._build_full_html_files_tree(Config.source_path, Config.out_pages_path)

    def start_autobuild(self):
        WikiTreeFilesWatcher(self.args.folder, 1, self).start()
        print(":: autobuild started")

    def _update_config(self):
        Config.name = self.args.name
        Config.theme = self.args.theme
        Config.source_path = Path(self.args.folder).absolute()
        Config.out_path = Path(self.args.output).absolute()
        Config.out_pages_path = Config.out_path / self.args.name / "pages"
        Config.out_styles_path = Config.out_path / self.args.name / "styles"

    def _create_output_folders(self) -> None:
        os.makedirs(Config.out_pages_path)
        os.makedirs(Config.out_styles_path)

    def _drop_output_folders(self) -> None:
        from shutil import rmtree

        try:
            rmtree(Config.out_path)
        except:
            pass

    def _copy_styles_by_theme(self) -> None:
        from distutils.dir_util import copy_tree

        available_themes = self.__get_available_themes()
        if Config.theme in available_themes:
            copy_tree(
                str(self.__get_theme_folder(Config.theme)), str(Config.out_styles_path)
            )

    def _build_list_of_html_files(
        self,
        list_of_source_paths: list[Path],
        out_folder: Path
    ):
        ...

    def _build_full_html_files_tree(self, source_folder: Path, out_folder: Path, root=1) -> None:
        subdirs = get_folders_subdirs(source_folder)
        files = get_folders_files(source_folder)

        for file in files:
            if is_md_file(file):
                add_page_file(source_folder / file, out_folder / md2html_extension(file))

        for subdir in subdirs:
            new_out_folder = out_folder / subdir
            new_source_folder = source_folder / subdir

            os.makedirs(new_out_folder)
            self._build_full_html_files_tree(new_source_folder, new_out_folder, root=0)

        add_index_page(source_folder, out_folder, bool(root))

    def __get_available_themes(self) -> list[str]:
        return os.listdir(Config.templates_folder)

    def __get_theme_folder(self, theme: str) -> Path:
        return Config.templates_folder / theme
