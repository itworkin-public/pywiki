import os
from pathlib import Path


def get_folders_files(path_to_folder: Path | str):
    return [
        name
        for name in os.listdir(path_to_folder)
        if not os.path.isdir(os.path.join(path_to_folder, name))
    ]


def get_folders_subdirs(path_to_folder: Path | str):
    return [
        name
        for name in os.listdir(path_to_folder)
        if os.path.isdir(os.path.join(path_to_folder, name))
    ]


def get_filename_from_path(path_to_file: Path | str):
    _, filename_with_extension = os.path.split(path_to_file)
    md_index = filename_with_extension.find(".md")
    return filename_with_extension[:md_index]


def get_folder_from_path(path: str | Path):
    return os.path.basename(path)
