import threading

from time import sleep
from pathlib import Path

from checksumdir import dirhash


class WikiTreeFilesWatcher:
    def __init__(
        self,
        wiki_source_path: Path | str,
        timeout: int,
        wiki_builder,
    ):
        self.__wiki_path = wiki_source_path
        self.__timeout = timeout

        self._last_dir_hash = ""
        self.__wiki_builder = wiki_builder

    def start(self):
        (
            threading.Thread(
                target=self.send_build_request_on_dir_change, 
                name="build wiki on updated")
            .start()
        )

    def send_build_request_on_dir_change(self):
        while True:
            sleep(self.__timeout)
            if self.is_wiki_updated():
                self.__wiki_builder.build_wiki()

    def is_wiki_updated(self):
        new_dir_hash = dirhash(self.__wiki_path)
        if self._last_dir_hash != new_dir_hash:
            self._last_dir_hash = new_dir_hash
            return True
        return False
